import bouquets.Bouquet;
import bouquets.FlowerPot;
import flowers.Camila;
import flowers.Flower;
import flowers.Lilac;
import flowers.Tulpin;

import java.util.Scanner;

public class Shop {

    /**
     * Function to choose and buy distinct flowers.
     */
    public void chooseOne() {

        final int tprice = 15;
        final int lprice = 10;
        final int clprice = 12;

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the choice: 1-tulip, 2-lilac, 3-camila, 0-nothing");

        int choice = scan.nextInt();
        System.out.println("How many?");
        int amount = scan.nextInt();

        switch (choice) {
            case 1:
                Flower tulpin = new Tulpin("red", tprice);
                tulpin.form();
                break;

            case 2:
                Flower lil = new Lilac("red", amount, lprice);
                System.out.println(lil.getPrice());
                lil.form();
                break;

            case 3:
                Flower caml = new Camila("lild", amount, clprice);
                caml.form();
                System.out.println(caml.getPrice());

            case 0:
                break;
        }
    }

    /**
     * Function to choose flowers and their
     * amount for bouquet.
     */
    public void chooseBouquet() {

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the size of bouquet: ");

        int size = scan.nextInt();
        System.out.println("How many tulpins, lilacs, camilas?");
        int nt = scan.nextInt();
        int nl = scan.nextInt();
        int nc = scan.nextInt();

        Flower boq = new Bouquet(nt, nl, nc);
        ((Bouquet) boq).setSize(size);
        System.out.println(boq.getPrice());
    }

    /**
     * Function to choose bouquet, which
     * consists only of one kind of flower.
     */
    public void choosewholeBoq() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the name and size of bouquet: ");
        String name = scan.next();
        int size = scan.nextInt();
        Flower boquet = new Bouquet(size, name);
    }

    /**
     * Function to choose and buy a pot.
     */
    public void choosePot() {
        final int price = 55;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter name, color, with proprerly price: ");
        String name = scan.next();
        String color = scan.next();
        FlowerPot pot = new FlowerPot(name, color, price);
    }

}
