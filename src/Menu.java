import java.util.Scanner;

public class Menu {
    private Shop shop;
    private Scanner scanner;

    public Menu(Shop shop, Scanner scanner) {
        this.shop = shop;
        this.scanner = scanner;
    }

    /**
     * Function to let user choose what to buy
     */
    public void chooseBouquet() {
        System.out.println("1 - buy flower. 2 - buy bouquet, 3 - choose flower for bouquet, 4 - buy flowerpot");
        int choice = this.scanner.nextInt();

        if (choice == 1) {
            shop.chooseOne();
        } else if (choice == 2) {
            shop.choosewholeBoq();
        } else if (choice == 3) {
            shop.chooseBouquet();
        } else if (choice == 4) {
            shop.choosePot();
        }
    }
}
