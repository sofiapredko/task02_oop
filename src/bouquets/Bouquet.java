package bouquets;

import flowers.Flower;

public class Bouquet extends Flower {

    private int ntulpin;
    private int nlilac;
    private int ncamila;

    private int size;
    private String name;

    public Bouquet(int size){
        this.size = size;
    }

    public Bouquet(int size, String name){
        this.size = size;
        this.name = name;
        System.out.println("You`ve chosen " + name +"bouquet" + "of" + size + "flowers");
    }

    public Bouquet(int ntulpin,int nlilac,int ncamila){
        this.ntulpin = ntulpin;
        this.nlilac = nlilac;
        this.ncamila = ncamila;
        System.out.println("You`ve chosen " + ntulpin + " tulpins, " + nlilac + " lilac, " + ncamila + " camila");
    }

    @Override
    public void form(){
        System.out.println("This is a bouquet");
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int getPrice(){
        System.out.print("Price: ");
        return size * 2;
    }
}

