package bouquets;

import flowers.Flower;

public class FlowerPot extends Flower {

    private String name;

    public FlowerPot(String name) {
        this.name = name;
        System.out.println("You`ve chosen " + name + "flowerpot");
    }

    public FlowerPot(String name, String color, int price) {
        super(color, price);
        System.out.println("You`ve chosen " + color + " " + name);
    }

    public FlowerPot(String name, String color, int amount, int price) {
        super(color, amount, price);
        System.out.println("You`ve chosen " + amount + " " + color + " " + name + " flowerpot");
    }

    @Override
    public void form() {
        System.out.println("It is a flowerpot");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
