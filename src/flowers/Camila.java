package flowers;

public class Camila extends Flower {

    public Camila(String color, int price) {
        super(color, price);
        System.out.println("You`ve chosen " + color + " camila");
    }

    public Camila(String color, int amount, int price) {
        super(color, amount, price);
        System.out.println("You`ve chosen " + amount + " " + color + " camaila");
    }

    @Override
    public void form(){
        System.out.println("It is a camila");
    }

}
