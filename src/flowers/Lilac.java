package flowers;

public class Lilac extends Flower {

    public Lilac(String color, int price) {
        super(color, price);
        System.out.println("You`ve chosen " + color + " lilac");
    }

    public Lilac(String color, int amount, int price) {
        super(color, amount, price);
        System.out.println("You`ve chosen " + amount + " " + color + " lilacs");
    }

    @Override
    public void form() {
        System.out.println("It is a lilac");
    }

}
