package flowers;

import java.util.Objects;

public abstract class Flower {

    private String color;
    private int amount;
    private int price;

    public Flower(String color, int price) {
        this.color = color;
        this.price = price;
        amount = 1;
    }

    public Flower(String color, int amount, int price) {
        this.color = color;
        this.amount = amount;
        this.price = price;
    }

    protected Flower() {
    }

    public abstract void form();

    public int getPrice() {
        System.out.print("Price: ");
        return price * amount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return amount == flower.amount &&
                price == flower.price &&
                Objects.equals(color, flower.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, amount, price);
    }
}
