package flowers;

public class Tulpin extends Flower {

    public Tulpin(String color, int price) {
        super(color, price);
        System.out.println("You`ve chosen " + color + " tulpin");
    }

    public Tulpin(String color, int amount, int price) {
        super(color, amount, price);
        System.out.println("You`ve chosen " + amount + " " + color + " tulpins");
    }

    @Override
    public void form() {
        System.out.println("It is a tulpin");
    }

}