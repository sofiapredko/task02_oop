import java.util.Scanner;

/**
 * @author Sofia
 *
 * Project that implements flower shop.
 */

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Shop shop = new Shop();
        Menu menu = new Menu(shop, scanner);

        boolean continueShopping = true;

        System.out.println("Welcome to the shop! How can I help you?");
        menu.chooseBouquet();

        while (continueShopping) {


           System.out.println("Something else? 0 - exit, 1 - continue ");
           boolean exit = scanner.nextInt() == 0;

           if (exit) {
               continueShopping = false;
               System.out.println("Good bye");
           } else {
               menu.chooseBouquet();
           }
        }
    }
}


